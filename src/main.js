import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import WelcomeTrivia from './components/WelcomeTrivia.vue'

Vue.config.productionTip = false
Vue.use(Router);
export default new Router({
  routes:[
    {path: '/',
  name: 'start',
  component: WelcomeTrivia},
  ]
})


new Vue({
  render: h => h(App),
}).$mount('#app')
